"""
@Author: Joris van Vugt, Moira Berens, Leonieke van den Bulk

Class for the implementation of the variable elimination algorithm.

"""
class Factor:

    def __init__(self, name):
        self.name = name
        self.table = [[]]


class VariableElimination:

    def __init__(self, network):
        """
        Initialize the variable elimination algorithm with the specified network.
        Add more initializations if necessary.

        """
        self.network = network
        self.factors = {}

    def run(self, query, observed, elim_order):
        """
        Use the variable elimination algorithm to find out the probability
        distribution of the query variable given the observed variables

        Input:
            query:      The query variable
            observed:   A dictionary of the observed variables {variable: value}
            elim_order: Either a list specifying the elimination ordering
                        or a function that will determine an elimination ordering
                        given the network during the run

        Output: A variable holding the probability distribution
                for the query variable

        """

    def reduce_variables(self, var, var_val):
        if var in self.network.variables:
            pass

    def sum_out(self):
        return

    def factorize(self):
        pass

#class Factor:
#
#    def __init__(self):

