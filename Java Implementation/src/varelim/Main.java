package varelim;

import java.util.ArrayList;

/**
 * Main class to read in a network, add queries and observed variables, and run variable elimination.
 * 
 * @author Marcel de Korte, Moira Berens, Djamari Oetringer, Abdullahi Ali, Leonieke van den Bulk
 */

public class Main {
	private final static String networkName = "test_files/win95pts.bif"; // The network to be read in (format and other networks can be found on http://www.bnlearn.com/bnrepository/)

	public static void main(String[] args) {
                //System.out.println(test.wd);
                //System.out.println(test.filenames.toString());
		// Read in the network
		Networkreader reader = new Networkreader(networkName); 
		
		// Get the variables and probabilities of the network
		ArrayList<Variable> vs = reader.getVs(); 
		ArrayList<Table> ps = reader.getPs(); 
		
		// Make user interface
		UserInterface ui = new UserInterface(vs, ps);
		
		// Print variables and probabilities
		ui.printNetwork();
		
		// Ask user for query and heuristic
		ui.askForQuery(); 
		Variable query = ui.getQueriedVariable(); 
		
		// Turn this on if you want to experiment with different heuristics for bonus points (you need to implement the heuristics yourself)
		//reader.askForHeuristic();
		//String heuristic = Ui.getHeuristic();
		
		// Ask user for observed variables 
		ui.askForObservedVariables(); 
		ArrayList<Variable> observed = ui.getObservedVariables(); 
                
                ui.askForHeuristic();
                String heuristic = ui.getHeuristic();
		
		// Print the query and observed variables
		ui.printQueryAndObserved(query, observed);
                System.out.println("Heuristic used: " + heuristic);
		
		
		//PUT YOUR CALL TO THE VARIABLE ELIMINATION ALGORITHM HERE
                Elimination elim = new Elimination(ps, query, observed, heuristic);
                //ArrayList<Variable> var = new ArrayList<>();
                //var.add(vs.get(2));
                //Factor factor = new Factor(ps.get(2));
				//System.out.println(ps.get(2) + "\n");
                //System.out.println(elim.sumOut(factor , vs.get(0)).getTable());
                elim.run();
	}
}

/**
 * Exception in thread "main" java.lang.NullPointerException
	at varelim.Table.toString(Table.java:39)
	at java.lang.String.valueOf(String.java:2994)
	at java.lang.StringBuilder.append(StringBuilder.java:131)
	at java.util.AbstractCollection.toString(AbstractCollection.java:462)
	at varelim.Main.main(Main.java:48)
C:\Users\Justin\AppData\Local\NetBeans\Cache\8.2\executor-snippets\run.xml:53: Java returned: 1
BUILD FAILED (total time: 4 seconds)
 */