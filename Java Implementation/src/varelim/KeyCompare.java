/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package varelim;

import java.util.Comparator;
import javafx.util.Pair;

/**
 * Comparator for integer variable pairs based on the integer value.
 * 
 * @author Floris Hendriks  s4749294
 * @author Justin Reniers   s4743474
 */
public class KeyCompare implements Comparator<Pair<Integer, Variable>> {

    @Override
    public int compare(Pair<Integer, Variable> o1, Pair<Integer, Variable> o2) {
        int index1 = o1.getKey();
        int index2 = o2.getKey();
        return index1 - index2;
    }
    
}
