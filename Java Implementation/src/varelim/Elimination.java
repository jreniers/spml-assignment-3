/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package varelim;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Set;
import javafx.util.Pair;

/**
 *
 * @author Floris Hendriks  s4749294
 * @author Justin Reniers   s4743474
 */
public class Elimination {

    private ArrayList<Variable> variables = new ArrayList<>();
    private ArrayList<Factor> factors = new ArrayList<>();
    private final Variable queried;
    private final ArrayList<Variable> observed = new ArrayList<>();
    private final String heuristic;
    private long startTime;
    private long stopTime;
    
    /**
     * Creates an elimination.
     * @param probabilities List of probability tables in Bayesian Network.
     * @param queried       Queried variable.
     * @param observed      List of observed variables for the query. 
     * @param heuristic     Heuristic for elimination ordering.
     */
    public Elimination(ArrayList<Table> probabilities, Variable queried, ArrayList<Variable> observed, String heuristic) {
        probabilities.forEach((Table table) -> {
            variables.add(table.getVariable());
            ArrayList<Variable> factor_vars = new ArrayList<>();
            factor_vars.add(table.getVariable());
            if (table.getVariable().hasParents())
                factor_vars.addAll(table.getParents());
            factors.add(new Factor(factor_vars, table));
        });
        this.queried = queried;
        this.observed.addAll(observed);
        this.heuristic = heuristic;
    }
    
    /**
     * Fixes an elimination ordering.
     * @param heuristic Heuristic to be used for ordering.
     * @return          List of variables in order of elimination (queried
     *                  variable not contained).
     */
    private ArrayList<Variable> fixOrder() {
        ArrayList<Variable> elim_order = new ArrayList<>();
        switch (this.heuristic.toLowerCase()) {
            case "least-incoming":
                elim_order.addAll(this.leastIncoming());
                break;
            case "fewest-factors":
                elim_order.addAll(this.inLeastFactors());        
                break;
            default:
                elim_order.addAll(this.variables);
                break;
        }
        this.variables.remove(this.queried);
        elim_order.remove(this.queried);
        System.out.println(elim_order.toString());
        return elim_order;
    }
    
    /**
     * Orders variables by least incoming arcs first.
     * @return  List of variables ordered by least incoming arcs first.
     */
    private ArrayList<Variable> leastIncoming() {
        PriorityQueue<Pair<Integer, Variable>> order = new PriorityQueue<>(new KeyCompare());
        for (Variable var : this.variables) {
            int parents = var.getNrOfParents();
            order.add(new Pair<>(parents, var));
        }
        ArrayList<Variable> elim_order = new ArrayList<>();
        while (!order.isEmpty()) {
            Pair<Integer, Variable> least = order.poll();
            elim_order.add(least.getValue());
        }
        return elim_order;
    }
    
    /**
     * Orders variables by contained in least factors first.
     * @return  List of variables ordered by contained in least factors first.
     */
    private ArrayList<Variable> inLeastFactors() {
        PriorityQueue<Pair<Integer, Variable>> order = new PriorityQueue<>(new KeyCompare());
        for (Variable var : this.variables) {
            int counter = 0;
            for (Factor factor : this.factors) {
                if (factor.getVariable().contains(var))
                    counter++;
            }
            order.add(new Pair<>(counter, var));
            System.out.println(counter + " " + var.getName());
        }
        ArrayList<Variable> elim_order = new ArrayList<>();
        while (!order.isEmpty()) {
            Pair<Integer, Variable> least = order.poll();
            elim_order.add(least.getValue());
        }
        return elim_order;
    }
    
    /**
     * Reduces probability tables of observed values by deleting rows in
     * probability tables for which observed value of variable "remove" is 
     * different than observed value.
     * @param remove    Variable that is observed.
     */
    private void reduce(Variable remove) {
        for (Factor factor : this.factors) {
            for (Variable var : factor.getVariable()) {
                if (var.getName().equals(remove.getName())) {                    
                    int index = factor.getIndex(remove);
                    Table table = factor.getTable();
                    Iterator it = table.getTable().iterator();
                    while (it.hasNext()) {
                        ProbRow row = (ProbRow)it.next();
                        if (index >= 0 && remove.getObserved() &&
                                !(row.getValues().get(index).equals(
                                remove.getObservedValue())))
                            it.remove();
                    }
                }
            }
        }
    }
        
    /**
     * Sums out rows of a factor where all values but the value of Variable var
     * are equal and creates new factor with them. Removes variable var from factor.
     * @param factor    Factor from which we add together rows.
     * @param var       Variable we sum out over.
     * @return          Factor with variable summed out.
     */
    private Factor sumOut(Factor factor, Variable var) {
        Table table = factor.getTable();
        int common_index = factor.getIndex(var);
        ArrayList<ProbRow> summed_out = new ArrayList<>();
        boolean[] match = new boolean[factor.getTable().size()];
        
        for (ProbRow row1 : table.getTable()) {
            double sum = row1.getProb();
            int index1 = table.getTable().indexOf(row1);
            if (!match[index1]) {
                for (ProbRow row2 : table.getTable()) {
                    int index2 = table.getTable().indexOf(row2);
                    if (common_index >= 0 && 
                            containsAll(row1, row2, common_index) && 
                            index1 != index2) {
                        sum += row2.getProb();
                        match[index2] = true;
                    }
                }
                summed_out.add(new ProbRow(row1.getValues(), sum));
                match[index1] = true;
            }
        }
        Factor f1 = remove_column(factor, var);
        Table table1;
        if (f1.getVariable().isEmpty())
            table1 = new Table(table.getVariable(), summed_out);  
        else 
            table1 = new Table(f1.getVariable().get(0), summed_out);
        Factor return_factor = new Factor(f1.getVariable(), table1);
        return return_factor;
    }

    /**
     * Removes the variable in the variables list of factor and the corresponding probabilities
     * @param factor Factor which we remove the column
     * @param var Variable that we want to remove in the factor and its probabilities
     * @return
     */
    private Factor remove_column(Factor factor, Variable var)
    {
        for(int i = 0; i < factor.getVariable().size(); i++) {
            if(factor.getVariable().get(i) == var) {
                factor.getVariable().remove(i);
                for(int j = 0; j < factor.getTable().size(); j++) {
                    factor.getTable().get(j).getValues().remove(i);
                }
            }
        }
        return factor;
    }

    /**
     * Checks if rows contain all equal values except for shared variable.
     * @param row1  First probability row.
     * @param row2  Second probability row.
     * @param index Index of shared variable. 
     * @return      True if all values but that of the shared variable are 
     *               equal, false otherwise.
     */
    private boolean containsAll(ProbRow row1, ProbRow row2, int index)
    {
        for(int i = 0; i < row1.getValues().size(); i++) {            
            if (!row1.getValues().get(i).equals(row2.getValues().get(i)) && i != index)
                return false;
        }
        return true;
    }
    
    /**
     * Finds all factors that contain variable "v" and adds them to a list.
     * @param v Variable checked for being in factors.
     * @return  List of factors containing variable "v".
     */
    private ArrayList<Factor> commonVariable(Variable v) {
        ArrayList<Factor> shared = new ArrayList<>();
        for (Factor factor : this.factors) {
            if (factor.getVariable().contains(v)) {
                shared.add(factor);
            }
        }
        return shared;
    }
    
    /**
     * Creates a list of symmetric different of ArrayList "l1" 
     * and ArrayList "l2".
     * @param l1    First list symmetric difference is calculated of.
     * @param l2    Second list symmetric difference is calculated of.
     * @return      List containing all variables in symmetric difference
     *              of "l1" and "l2".
     */
    private ArrayList<Variable> symmetricDifference(ArrayList<Variable> l1, ArrayList<Variable> l2) {
        Set<Variable> union = new HashSet<>(l1);
        union.addAll(l2);
        Set<Variable> intersection = new HashSet<>(l1);
        intersection.retainAll(l2);
        union.removeAll(intersection);
        ArrayList<Variable> temp = new ArrayList<>();
        temp.addAll(union);
        return temp;
    }
    
    /**
     * Creates a list of indices for variables in "common" and also in "factor"
     * where index is index of common variable in variable list of factor.
     * @param common    List of variables that are shared between all factors of
     *                  current factorize step.
     * @param factor    Factor of which indices of common variables are needed.
     * @return          List of indices for common variables in factor.
     */
    private ArrayList<Integer> indexList(ArrayList<Variable> common, ArrayList<Variable> factor) {
        ArrayList<Integer> indices = new ArrayList<>();
        for (Variable shared : common) {
            for (Variable var : factor) {
                if (shared.getName().equals(var.getName()))
                    indices.add(factor.indexOf(var));
            }
        }
        return indices;
    }
    
    /**
     * Multiplies two factors based on common variables between the two factors:
     * creates a new probability row if all values of common variables in row 1 
     * of factor 1 are equal to all values of common variables in row 2 of 
     * factor 2. Creates new factor for union of variables of factor 1 and 2
     * and probability table containing all new probability rows created.
     * @param f1    Factor 1 to be multiplied.
     * @param f2    Factor 2 to be multiplied.
     * @return      New factor that results from multiplication of "f1" and "f2".
     */
    private Factor multiply(Factor f1, Factor f2) {
        ArrayList<ProbRow> new_factor_table = new ArrayList<>();
        HashSet<Variable> intersection = new HashSet<>(f1.getVariable());
        HashSet<Variable> var2 = new HashSet<>(f2.getVariable());
        intersection.retainAll(var2);       
        ArrayList<Variable> symmetric_difference = 
                this.symmetricDifference(f1.getVariable(), f2.getVariable());
        ArrayList<Variable> union = new ArrayList<>();
        union.addAll(intersection);
        union.addAll(symmetric_difference);
        
        ArrayList<Variable> common = new ArrayList<>(intersection);        
        
        ArrayList<ProbRow> f1probs = f1.getTable().getTable();
        ArrayList<ProbRow> f2probs = f2.getTable().getTable();
        
        ArrayList<Integer> f1indices = this.indexList(common, f1.getVariable());
        ArrayList<Integer> f2indices = this.indexList(common, f2.getVariable());
        
        for (ProbRow r1 : f1probs) {
            for (ProbRow r2 : f2probs) {
                boolean add = true;
                for (Variable intersect : common) {
                        
                    int index = common.indexOf(intersect);
                    if (!add)
                        break;
                    add = r1.getValues().get(f1indices.get(index)).equals(r2.getValues().get(f2indices.get(index)));
                }
                if (add) {
                    ArrayList<String> values = new ArrayList<>();
                    for (Variable un : union) {
                        if (f1.getVariable().contains(un))
                            values.add(r1.getValues().get(f1.getVariable().indexOf(un)));
                        else
                            values.add(r2.getValues().get(f2.getVariable().indexOf(un)));
                    }
                    new_factor_table.add(new ProbRow(values, r1.getProb() * r2.getProb()));
                }
            }
        }
        ArrayList<Variable> initial = new ArrayList<>();
        initial.add(union.get(0));
        Factor factor = new Factor(initial, new Table(union.get(0), new_factor_table));
        for (Variable var : union) {
            factor.addVariables(var);
        }
        return factor;
    }
    
    /**
     * Normalizes factor.
     * @param factor    Factor to be normalized.
     * @return          Normalized factor.     
     */
    private Factor normalize(Factor factor) {
        double sum = 0;
        ArrayList<ProbRow> normalized_probs = new ArrayList<>();
        for (ProbRow row : factor.getTable().getTable()) {
            sum += row.getProb();
        }
        for (ProbRow row : factor.getTable().getTable()) {
            normalized_probs.add(new ProbRow(row.getValues(), row.getProb()/sum));
        }
        ArrayList<Variable> factor_vars = new ArrayList<>();
        factor_vars.addAll(factor.getVariable());
        Factor normalized_factor = new Factor(factor_vars, new Table(factor.getVariable().get(0), normalized_probs));
        for (Variable var : factor.getVariable())
            normalized_factor.addVariables(var);
        return normalized_factor;
    }
    
    /**
     * Prints all factors currently in Bayesian Network.
     */
    public void printNetwork() {
        System.out.println("The probabilities are:\n");
        for (Factor factor : this.factors) {
            System.out.println(factor.getVariable() + " " + factor.getTable().toString());
        }
    }
    
    /**
     * Variable Elimination algorithm: 
     * @param reduction_order   Order in which variables are eliminated from 
     *                          factors.
     * @return                  Return factor received after eliminating all
     *                          variables in elimination order.
     */
    private Factor factorize(ArrayList<Variable> reduction_order) {
        Factor end;
        for (Variable var : reduction_order) {
            boolean multiplied = false;
            Factor result;
            ArrayList<Factor> commons;
            commons = this.commonVariable(var);
            while (commons.size() > 1) {
                multiplied = true;
                Factor f1 = commons.get(0);
                Factor f2 = commons.get(1);
                Factor new_f = this.multiply(f1, f2);
                this.factors.remove(f1);
                this.factors.remove(f2);
                commons.remove(f1);
                commons.remove(f2);
                commons.add(new_f);
            }
            if (commons.size() == 1) {
                if (!multiplied)
                    this.factors.remove(commons.get(0));
                result = this.sumOut(commons.get(0), var);
                this.factors.add(result);
            }
        }
        while (this.factors.size() > 1) {
            Factor f1 = this.factors.get(0);
            Factor f2 = this.factors.get(1);
            Factor remaining = this.multiply(f1, f2);
            this.factors.remove(f1);
            this.factors.remove(f2);
            this.factors.add(remaining);
        }
        end = this.factors.get(0);
        return end;
    }
    
    /**
     * Variable Elimination algorithm:
     *  Reduces observed variables, creates an elimination ordering, factorizes
     *  all factors with elimination ordering, normalizes result and prints 
     *  running time and resulting factor.
     */
    public void run() {
        for (Variable obs : observed)
            reduce(obs);
        ArrayList<Variable> reduction_order;
        reduction_order = this.fixOrder();
        this.startTime = System.currentTimeMillis();
        Factor result = this.factorize(reduction_order);
        result = this.normalize(result);
        this.stopTime = System.currentTimeMillis();
        this.printImportantResults(result);
    }
    
    /**
     * Prints variables of factor and probability table of factor, running
     * time for running Variable Elimination algorithm, queried variable,
     * observed variables (and their observed value) and heuristic used.
     * @param factor    Factor of which we want to print information.
     */
    private void printImportantResults(Factor factor) {
        System.out.println("Resulting factor: \t" + factor.getVariable()
                        + factor.getTable().getTable() +"\nRuntime (seconds): \t"
                        + (this.stopTime - this.startTime)/1000.
                        + "\nQueried variable: \t" + this.queried);
        String observed_str = "Observed variable(s): \n";
        for (Variable ob : this.observed) {
            observed_str += "\t\t\t" + ob.getName() + ob.getObservedValue() + "\n";
        }
        System.out.println(observed_str + "Heuristic used: \t" + this.heuristic);
    }
}