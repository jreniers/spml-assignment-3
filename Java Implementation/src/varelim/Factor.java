/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package varelim;
import java.util.ArrayList;

/**
 * 
 * @author Floris Hendriks  s4749294
 * @author Justin Reniers   s4743474
 */

public class Factor {
    
    private final ArrayList<Variable> variables = new ArrayList<>();
    private final Table table;
 
    /**
     * Creates a factor.
     * @param var   All variables contained in the factor.
     * @param table Probability table of the factor.
     */
    public Factor(ArrayList<Variable> var, Table table) {
        this.variables.addAll(var);
        this.table = table;
    }

    /**
     * Adds parent variable to variable list of factor.
     * @param var   Parent variable to be added.
     * @return      True if adding was successful, false if adding failed.
     */
    public boolean addParent(Variable var)
    {
        if(var.hasParents())
        {
            for(int i = 0; i < var.getParents().size(); i++)
            {
                this.variables.add(var.getParents().get(i));
            }
            return true;
        }
        return false;
    }

    /**
     * Getter for variables of factor.
     * @return  ArrayList of variables of this factor.
     */
    public ArrayList<Variable> getVariable() {
        return this.variables;
    }
    
    /**
     * Getter for probability table of factor.
     * @return  Table (probability values) of this factor.
     */
    public Table getTable() {
        return this.table;
    }
    
    /**
     * Index lookup of variable in list of factor variables.
     * @param var   Variable to look up.
     * @return      Index of variable if variable in factor, -1 otherwise.
     */
    public int getIndex(Variable var) {
        if (variables.contains(var)) 
            return variables.indexOf(var);
        return -1;
    }
    
    /**
     * Adds variable to factor variable list.
     * @param var   Variable to be added to the factor.
     * @return      True if variable is added, false if adding failed or 
     *              variable is already in factor.
     */
    public boolean addVariables(Variable var) {
        if (!variables.contains(var))
            return variables.add(var);
        return false;
    }
    
    /**
     * Removes variable from factor variable list.
     * @param var   Variable to be removed from the factor.
     * @return      True if variable is removed, false if removing failed or
     *              variable wasn't in factor.
     */
    public boolean removeVariables(Variable var) {
        if (variables.contains(var))
            return variables.remove(var);
        return false;
    }
}
